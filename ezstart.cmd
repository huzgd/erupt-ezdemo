@echo off

:start0

cd /d %~dp0
cd

echo ================
echo Erupt-EzDML-Demo
echo ================
echo 说明：本示例基于Erupt（spring-jpa），需要本机安装JDK8，可用idea、eclipse导入运行，也可以直接使用本命令菜单，本命令使用mvnw.cmd下载安装所需内容并运行。
echo 本工程已上传到：https://gitee.com/huzgd/erupt-ezdemo
echo Erupt框架文档：https://www.erupt.xyz/
echo 如果本机没有maven相关环境，首次运行mvnw install可能需要从外网下载，需要5-10分钟甚至更久，请耐心等待。
echo ...
echo 操作菜单：
echo 1.执行mvnw install，下载安装编译打包
echo 2.运行target/jar，启动服务
echo 3.进入命令行
echo 4.打开资源管理器
echo 5.删除测试数据库
echo 6.退出
echo ...
choice /c 123456 /m "请选择操作"

if %errorlevel%==1 goto fun1
if %errorlevel%==2 goto fun2
if %errorlevel%==3 goto fun3
if %errorlevel%==4 goto fun4
if %errorlevel%==5 goto fun5
if %errorlevel%==6 goto fun6

:fun1
echo ...
echo 执行mvnw install命令，编译打包生成jar（首次运行会下载安装引用支撑包可能需要较长时间）...
echo mvnw install ...
call mvnw install
goto start0

:fun2
echo ...
echo 运行target/jar，启动服务...
start http://localhost:8080/
call java -jar target\ezdemo-0.0.1-SNAPSHOT.jar
goto start0


:fun3
cmd
goto start0

:fun4
start explorer %~dp0
goto start0

:fun5
echo 删除测试数据库 ezerupt*.db...
del ezerupt*.db
echo done!
goto start0

:fun6
cd ..
echo 正在退出...
choice /t 1 /d y /n>nul

