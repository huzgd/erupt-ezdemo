#!/bin/bash

cd "$(dirname $0)"
echo "$(dirname $0)"
pwd

#Shell菜单
while true; do

cat << EOF
================
Erupt-EzDML-Demo
================
说明：本示例基于Erupt（spring-jpa），需要本机安装JDK8，可用idea、eclipse导入运行，也可以直接使用本命令菜单，本命令使用mvnw.cmd下载安装所需内容并运行。
本工程已上传到：https://gitee.com/huzgd/erupt-ezdemo
Erupt框架文档：https://www.erupt.xyz/
如果本机没有maven相关环境，首次运行mvnw install可能需要从外网下载，需要5-10分钟甚至更久，请耐心等待。
...
1)执行mvnw install，下载安装编译打包
2)运行target/jar，启动服务
3)进入命令行
4)退出

EOF

  read -p "请选择操作：" num1
  case $num1 in
   1)
    echo ...
    echo 执行mvnw install命令，编译打包生成jar（首次运行会下载安装引用支撑包可能需要较长时间）...
    echo ./mvnw install ...
    chmod 777 mvnw
    ./mvnw install
    ;;
   2)
    echo ...
    echo 运行target/jar，启动服务...
    java -jar ./target/ezdemo-0.0.1-SNAPSHOT.jar
    ;;
   3)
    bash
    ;;
   4)
    exit 0
  esac

done
