<%
//ezdml model config js

var md=curModel;
var mPkg=AutoCapProc(md.name,'JavaPackageName');

var parentPkgName=GetGParamValue('EZGEN_ROOT_PKGNAME'); //上级包名
if(!parentPkgName)parentPkgName='com.ezdml.erupt.ezdemo';

%>package ${parentPkgName}.${mPkg};


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import xyz.erupt.core.annotation.EruptScan;
import xyz.erupt.core.module.EruptModule;
import xyz.erupt.core.module.EruptModuleInvoke;
import xyz.erupt.core.module.MetaMenu;
import xyz.erupt.core.module.ModuleInfo;
import java.util.ArrayList;
import java.util.List;


<%
  for(var j=0; j<md.tables.count; j++)
  {
    var tb = md.tables.getItem(j);
    if(!tb.isChecked || (tb.typeName == 'TEXT') )
      continue;
%>
import ${parentPkgName}.${mPkg}.${AutoCapProc(tb.name,'JavaPackageName')}.${AutoCapProc(tb.name,'ClassNameSafe')};
<%
  }
%>

/**
 * @author huz
 * date ${_date()}
 */
@Configuration
@ComponentScan
@EntityScan
@EruptScan
@Component
public class Erupt${AutoCapProc(md.name,'ClassName')}AutoConfiguration implements EruptModule {

    static {
        EruptModuleInvoke.addEruptModule(Erupt${AutoCapProc(md.name,'ClassName')}AutoConfiguration.class);
    }

    @Override
    public ModuleInfo info() {
        return ModuleInfo.builder().name("erupt_${AutoCapProc(md.name,'PackageName')}").build();
    }

    @Override
    public List<MetaMenu> initMenus() {
        List<MetaMenu> menus = new ArrayList<>();
        menus.add(MetaMenu.createRootMenu("$ez${mPkg}", "${md.displayText}", "fa fa-code", 40));
        
<%
  for(var j=0; j<md.tables.count; j++)
  {
    var tb = md.tables.getItem(j);
    if(!tb.isChecked || (tb.typeName == 'TEXT') )
      continue;
%>
        menus.add(MetaMenu.createEruptClassMenu(${AutoCapProc(tb.name,'ClassNameSafe')}.class, menus.get(0), 0));
<%
  }
%>

        return menus;
    }
}
